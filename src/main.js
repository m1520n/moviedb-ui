// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueRx from 'vue-rx'
import { Observable } from 'rxjs/Observable'
import { Subject } from 'rxjs/Subject'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'

Vue.use(VueMaterial)

Vue.config.productionTip = false

Vue.material.registerTheme('about', {
  primary: {
    color: 'blue-grey',
    hue: 'A700'
  }
})

Vue.material.setCurrentTheme('about')

Vue.use(VueRx, {
  Observable,
  Subject
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
