import * as nock from 'nock'
import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'

const expect = chai.expect
chai.use(chaiAsPromised)

import config from '../config'
import movieDbService from '@/services/moviedb.service'

describe('MovieDB service', () => {
  nock(config.base_uri)
    .get('search/movie/', (body) => body.query === 'query' && body.api_key === config.api_key)
    .reply(200, {
      results: [{}, {}, {}]
    })

  describe('wrapInPromise method', () => {
    it('should return function wrapped in a promise', () => {
      return expect(movieDbService.wrapInPromise(function () {}))
        .to.eventually.equal('woof')
    })
  })

  describe('findMovies method', () => {
    it('should return MovieDB API response', () => {
      return expect(movieDbService.findMovies(function () {}))
        .to.eventually.equal('woof')
    })
  })
})
