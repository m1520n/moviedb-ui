import theMovieDb from '../assets/js/themoviedb'
import config from '../config'

theMovieDb.common.api_key = config.api_key

/**
 * Wraps movieDb library methods in a Promise
 * @param {Function} fn - method to be wrapped in a promise
 * @param {Object} options - object containing arguments
 * @return {Promise} - Promise wrapper
 */
function wrapInPromise (fn, options) {
  return new Promise((resolve, reject) => {
    fn(options, result => {
      resolve(JSON.parse(result))
    }, error => {
      reject(error)
    })
  })
}

/** Returns response containing
 * @param {String} - query string
 * @return {Promise} - response Promise
 */
function findMovies (query) {
  return wrapInPromise(theMovieDb.search.getMovie, { query })
}

export {
  wrapInPromise,
  findMovies
}
