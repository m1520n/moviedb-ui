/**
 * MovieDB library configuration params
 */

export default {
  api_key: '154d186abeefafe66a4d3a81d044260e',
  base_uri: 'https://api.themoviedb.org/3/',
  images_uri: 'https://image.tmdb.org/t/p/',
  timeout: 5000
}
